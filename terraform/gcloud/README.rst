Google Cloud with Terraform
===========================
The terraform script to create a publicly accessible instance here is pretty straight forward. I will continue using this
deployment.

Requirements
------------
* you need to copy the public part of your SSH key (id-ed25519.pub in my case) and put it here.
* there should be a file called ``account.json`` as well. In order to obtain this file, check out: https://www.terraform.io/docs/providers/google/guides/getting_started.html#adding-credentials

Instructions
------------
In order to create the instance at GCE, you just need to:

.. code-block:: sh

    terraform plan
    terraform apply --auto-approve

When you're done with it, you can destroy the created resources with:

.. code-block:: sh

    terraform destroy

.. warning::
   Make sure you're done with it. `ALL RESOURCES WILL BE DESTROYED`
