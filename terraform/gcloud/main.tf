provider "google" {
  credentials = file("account.json")
  project = "grainchain-techtest"
  region = "us-central1"
  zone = "us-central1-a"
}

resource "google_compute_instance" "gc-tech-test" {
  name = "gc-tech-test"
  machine_type = "n1-highcpu-8"
  zone = "us-central1-a"
  tags = ["web-server"]

  metadata = {
    ssh-keys = "root:${file("id_ed25519.pub")}"
  }

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-minimal-2004-lts"
      size = 32
    }
  }

  network_interface {
    network = "default"
	access_config {
    }
  }
}

resource "google_compute_firewall" "web-server" {
  name = "default-allow-web"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["80", "443", "16443"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["web-server"]
}
