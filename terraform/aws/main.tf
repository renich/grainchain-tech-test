provider "aws" {
  version = "~> 2.0"
  region  = "us-west-2"
	shared_credentials_file = ".creds"
}

resource "aws_vpc" "gc-vpc" {
	cidr_block = "10.0.0.0/16"
}

resource "aws_subnet" "gc-subnet" {
	vpc_id = aws_vpc.gc-vpc.id
	cidr_block = "10.0.1.0/24"
	availability_zone = "us-west-2a"
	map_public_ip_on_launch = true
}

resource "aws_network_interface" "gc-nic0" {
	subnet_id = aws_subnet.gc-subnet.id
	private_ips = ["10.0.1.32"]
}

resource "aws_internet_gateway" "gc-gw" {
	vpc_id = aws_vpc.gc-vpc.id
}

resource "aws_instance" "gc-tech-test" {
	ami = "ami-06f2f779464715dc5"
	instance_type = "t3a.2xlarge"
	key_name = "gc-tech-test"
	vpc_security_group_ids = [aws_security_group.gc-sg.id]

	root_block_device {
		volume_size = "32"
	}

	network_interface {
		network_interface_id = aws_network_interface.gc-nic0.id
		device_index = 0
	}

	depends_on = [aws_internet_gateway.gc-gw]
}

resource "aws_security_group" "gc-sg" {
	name = "allow_ssh"
	description = "Allow port 22/tcp inbound traffic"
	vpc_id = aws_vpc.gc-vpc.id

	ingress {
		description = "SSH from VPC"
		from_port = 22
		to_port = 22
		protocol = "tcp"
		cidr_blocks = ["0.0.0.0/0"]
	}

	egress {
		from_port = 22
		to_port = 22
		protocol = "tcp"
		cidr_blocks = ["0.0.0.0/0"]
	}
}

resource "aws_eip" "gc-eip" {
	vpc = true

	depends_on = [aws_internet_gateway.gc-gw]
}

resource "aws_lb" "gc-lb" {
	name = "gc-lb"
	internal = false
	load_balancer_type = "network"

	subnet_mapping {
		subnet_id = aws_subnet.gc-subnet.id
		allocation_id = aws_eip.gc-eip.id
	}
}


