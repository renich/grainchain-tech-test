MicroK8S
========
This ansible playbook installs ``microk8s`` and configures a ``kubectl`` and ``helm`` aliases for the root user.

Pre-requisites
==============
* Ubuntu 20.04 installed
* ssh key-based access as root to the server.

Instructions
------------
Just edit `inventory/gc-tech-test.yaml` to point to the host you want to configure. Make sure to edit the ``ansible_host`` as well
(or remove it if hte hostname is resolvable).

Then, just run the playbook as follows:

.. code-block:: sh

    ansible-playbook site.yaml

This will run two roles.

common:
    installs ``microk8s`` using snap and configures the kubectl alias.

helm3:
    Enables the ``helm3`` addon provided by ``microk8s``. It, also, configures the alias for it.

After that, you can just use it normally.
