FROM nginx:mainline-alpine
COPY app/src /usr/share/nginx/html
COPY nginx/nginx.conf /etc/nginx/
EXPOSE 5000
